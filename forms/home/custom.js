$(function () {
    //Limitador de caracteres
    $('input').attr('maxLength','50')

    

    //UX ajuste status do fornecedor
    var status = $('#statusfornecedor');
    status.change(function () {
        if (status.val() == "") {
            status.addClass("error");
            status.removeClass('valid');
        }
    }) 
    //UX ajuste formula
     var formula = $('#formula');
     formula.change(function () {
         if (formula.val() == "") {
             formula.addClass("error");
             formula.removeClass('valid');
         }
     })
    //UX ajuste nacionalidade
    var nac = $('#nacionalidade');
    nac.change(function () {
        if (nac.val() == "") {
            nac.addClass("error");
            nac.removeClass('valid');
        }
    })
     //UX ajuste tipo
     var tipo = $('#tipo');
     tipo.change(function () {
         if (tipo.val() == "") {
             tipo.addClass("error");
             tipo.removeClass('valid');
         }
     })

    // CEP só números
    $("#cep").keyup(function () {
        $("#cep").val(this.value.match(/[0-9]*/));
    });

    //Máscara para CPF ** jquery Mask
    $("#cpf").mask("000000000-00");
    $("#cnpj").mask("99.999.999/9999-99");
    $("#pis").mask("999.99999.99-9");

    //Dependendo do select aparece CPF ou CNPJ
    $('#tipo').change(function () {
        var tipo = $('#tipo').val()

        if (tipo == 'pessoafisica') {
            $('#cpfcnpj').fadeIn()
            $('#cpf').fadeIn()
            $('#labelcpf').fadeIn()
            $('#cnpj').hide()
            $('#labelcnpj').hide()
            $('#divcpf').fadeIn()
            $('#divcnpj').hide()
        }
        if (tipo == '') {
            $('#cpfcnpj').fadeOut()
        }
        if (tipo == 'pessoajuridica') {
            $('#cpfcnpj').fadeIn()
            $('#cnpj').fadeIn()
            $('#labelcnpj').fadeIn()
            $('#cpf').hide()
            $('#labelcpf').hide()
            $('#divcnpj').fadeIn()
            $('#divcpf').hide()

        }
    });


    //Se tiver preenchido banco aparecer conta
    $('#banco').blur(function () {

        if ($('#banco').val() != '' && $('#banco').val() != 0) {

            $("#divconta").fadeIn()

        } else {
            $("#divconta").fadeOut()
        }
    });

});